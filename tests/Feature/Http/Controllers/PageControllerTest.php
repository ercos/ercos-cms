<?php

namespace Ercos\ErcosCms\Tests\Feature\Http\Controllers;

use Ercos\ErcosCms\Tests\TestCase;

class PageControllerTest extends TestCase
{
    public function test_preview()
    {
        $this->assertTrue(true);
    }

    public function test_show()
    {
        $this->assertTrue(true);
    }

    public function test_index()
    {
        $response = $this->get('/api/pages');
        $response->assertStatus(200);
    }

    public function test_listPublishedPageUrls()
    {
        $this->assertTrue(true);
    }
}
