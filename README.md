# CMS based on Laravel and FilamentPHP.

[![Latest Version on Packagist](https://img.shields.io/packagist/v/ercos/ercos-cms.svg?style=flat-square)](https://packagist.org/packages/ercos/ercos-cms)
[![Total Downloads](https://img.shields.io/packagist/dt/ercos/ercos-cms.svg?style=flat-square)](https://packagist.org/packages/ercos/ercos-cms)

The Filament Headless CMS package provides a simple and easy to use CMS for Laravel applications.

## Features

- Pages : including SEO fields, page blocks, page preview and drafts
- Menus : allows to create and manage menus and sub-menus (with pages, external links, ...) and footer content
- Tiny MCE editor
- Font Awesome Service (Icon Picker)

As this is intended to be headless, we do not provide any frontend views.
You can use the provided data to build your own frontend [using the API](#api-routes) or [Laravel functions](#useful-functions-when-working-with-laravel).

## Core

### Installation

Install the package via composer :

```bash
composer require ercos/ercos-cms
```


Run the plugin install command.
```php
php artisan ercos-cms:install
```

Publish the assets
```bash
php artisan filament:assets
```

Finally, start adding page sections using the command `ercos-cms:make:section {sectionName}`.

### Usage

Add the plugin to your Filament installation.

```php
class AdminPanelProvider extends PanelProvider
{
    public function panel(Panel $panel): Panel
    {
        return $panel
            ...
            ->plugins([
                \Ercos\ErcosCms\Filament\Plugin\CmsPlugin::make()
                    ->navigationGroup('Structure')
                    ->registerNavigation(true) // Optional: Either show the navigation items or not
            ]);
```

## API routes

- GET /api/pages : list of pages and their content
- GET /api/pages/preview : preview of a page
- GET /api/pages/published-urls : list of published page urls (useful for SSG)
- GET /api/pages/{slug} : get a page by its slug
- GET /api/sitemap : get the sitemap
- GET /api/menus : list of menus and their content

 ## Useful functions when working with Laravel

- Ercos\ErcosCms\Facades\ErcosCms::getPages() : get all pages
- Ercos\ErcosCms\Facades\ErcosCms::getMenus() : get all menus

## TinyMCE 

### Installation

Add your TinyMCE script in the config file 
```php
    'tiny_mce' => [
        'script' => 'https://cdn.tiny.cloud/1/********/tinymce/7/tinymce.min.js',
        ...
    ]
    'tiny-mce' => 'https://cdn.tiny.cloud/1/********/tinymce/7/tinymce.min.js'
```

### Usage

```php
TinyMceEditor::make('...')
    ->label('...')
    ...
```

## FontAwesome

### Installation

Add your Fontawesome css file in the config
```php
    'fontawesome' => 'https://kit.fontawesome.com/********.css'
```

### Usage

You may use fontawesome in a Select to choose an icon. 
```php
Select::make('...')
    ->label('...')
    ->reactive()
    ->searchable()
    ->allowHtml()
    ->getSearchResultsUsing(
        fn(string $search) => App::make(FontawesomeService::class)
            ->searchFontawesomeIconNamesOptions($search)
            ->pluck('label', 'value')
    )
    ->getOptionLabelUsing(
        fn($value) => $value
            ? App::make(FontawesomeService::class)->getHtmlElement($value)
            : null
    )
    ->preload(false)
    ...
```

In case you use TinyMCE, fontawesome will also be available.
![alt text](https://bitbucket.org/ercos/ercos-cms/raw/dd759e2e791f06e29268296960d27ed72b5cece4/fontawesome-plugin.png)

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Ercos](https://github.com/tbornick)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
