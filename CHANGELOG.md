# Changelog

All notable changes to `ercos-cms` will be documented in this file.

## 1.6.1 - 2024-05-21

- fix preview url + live url

## 1.6 - 2024-05-21

- add meta prop to the menu items

## 1.6 - 2024-05-21

- add meta prop to the menu items

## 1.5.1 - 2024-05-15

- fix menu facade

## 1.5.1 - 2024-05-14

- fix menu items with anchor links

## 1.5.0 - 2024-05-12

- allow anchor links in the menu

## 1.4.1 - 2024-05-08

- fix Page Type select always active

## 1.4.0 - 2024-05-01

- added macros `drafts` and `dropDrafts` for migrations
- ability to handle slug routes with /{slug} or :slug syntax (depending on the front-end framework used)

## 1.3.5 - 2024-04-23

- added facade function `getPageByUuid()`

## 1.3.4 - 2024-04-23

- fix TinyMCE when having multiple instanced in the same page

## 1.3.3 - 2024-04-23

- added configuration to hide the navigation items `->registerNavigation([bool|Closure])`

## 1.3.2 - 2024-04-19

- update TinyMceEditor configuration to customize block_formats and toolbar commands

## 1.3.1 - 2024-04-19

- update TinyMceEditor to be compatible with the latest TinyMce version (7)
- update the plugin install process

## 1.3.0 - 2024-04-17

- added functions getPages() and getMenus() to ErcosCms facade

## 1.2.0 - 2024-04-16

- added command `php artisan ercos-cms:create-section {sectionName}` to create a new section

## 1.1.1 - 2024-04-16

- fix dépendances de tiny-mce

## 1.1.0 - 2024-04-16

- ajout du composant d'édition tiny-mce `TinyMceEditor`
- ajout du service de recherche d'icônes Fontawesome `FontawesomeService`

## 1.0.5 - 2024-04-03

- fix footer links return type

## 1.0.4 - 2024-04-03

- fix "Duplicate a page"

## 1.0.3 - 2024-04-03

- fix "Duplicate a page"
- reverse pages actions order "Delete" et "Duplicate"
