
<x-dynamic-component
        :component="$getFieldWrapperView()"
        :field="$field"
>
    <div
            x-data="{ state: $wire.$entangle('{{ $getStatePath() }}') }"

            x-cloak
            class="overflow-hidden"
            wire:ignore
    >
        <!-- Interact with the `state` property in Alpine.js -->
        <textarea
                x-model="state"
                id="tiny-mce-editor-{{ str_replace ('.', '', $getId()) }}"
        ></textarea>
        <div x-init="(() => {
        tinymce.init({
                selector: '#tiny-mce-editor-{{ str_replace ('.', '', $getId()) }}',
                content_css: '{{ config('ercos-cms.fontawesome') ?: '' }}',
                noneditable_noneditable_class: 'far',
                language: 'fr_FR',
                block_formats: '{{ config('ercos-cms.tiny_mce.block_formats') }}',
                menubar: false,
                branding: false,
                convert_urls: false,
                extended_valid_elements: 'span[*]',
                plugins: '{{ config('ercos-cms.fontawesome') ? 'fontawesome' : '' }} advlist codesample directionality emoticons fullscreen image link lists media table',
                toolbar: '{{ config('ercos-cms.tiny_mce.toolbar') }}',
                images_upload_handler: (blobInfo, success, failure, progress) => {
                    if (!blobInfo.blob()) return

                    $wire.upload(`componentFileAttachments.{{ $getStatePath() }}`, blobInfo.blob(), () => {
                        $wire.getFormComponentFileAttachmentUrl('{{ $getStatePath() }}').then((url) => {
                            if (!url) {
                                failure('{{ __('Error uploading file') }}')
                                return
                            }
                            success(url)
                        })
                    })
                },
                file_picker_callback: (cb, value, meta) => {
                    const input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.addEventListener('change', (e) => {
                        const file = e.target.files[0];
                        const reader = new FileReader();
                        reader.addEventListener('load', () => {
                            $wire.upload(`componentFileAttachments.{{ $getStatePath() }}`, file, () => {
                                $wire.getFormComponentFileAttachmentUrl('{{ $getStatePath() }}').then((url) => {
                                    if (!url) {
                                        cb('{{ __('Error uploading file') }}')
                                        return
                                    }
                                    cb(url)
                                })
                            })
                        });
                        reader.readAsDataURL(file);
                    });

                    input.click();
                },
                automatic_uploads: true,
                setup: function(editor) {
                        if(!window.tinySettingsCopy) {
                            window.tinySettingsCopy = [];
                        }

                        editor.on('blur', function(e) {
                            state = editor.getContent()
                        })

                        editor.on('init', function(e) {
                            if (state != null) {
                                editor.setContent(state)
                            }
                        })

                        editor.on('OpenWindow', function(e) {
                            target = e.target.container.closest('.fi-modal')
                            if (target) target.setAttribute('x-trap.noscroll', 'false')
                        })

                        editor.on('CloseWindow', function(e) {
                            target = e.target.container.closest('.fi-modal')
                            if (target) target.setAttribute('x-trap.noscroll', 'isOpen')
                        })

                        function putCursorToEnd() {
                            editor.selection.select(editor.getBody(), true);
                            editor.selection.collapse(false);
                        }

                        $watch('state', function(newstate) {
                            // unfortunately livewire doesn't provide a way to 'unwatch' so this listener sticks
                            // around even after this component is torn down. Which means that we need to check
                            // that editor.container exists. If it doesn't exist we do nothing because that means
                            // the editor was removed from the DOM
                            if (editor.container && newstate !== editor.getContent()) {
                                editor.resetContent(newstate || '');
                                putCursorToEnd();
                            }
                        });
                    },
            });
        })"></div>
    </div>
</x-dynamic-component>
