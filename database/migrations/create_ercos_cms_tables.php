<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        if (!Schema::hasTable('pages')) {
            Schema::create('pages', function (Blueprint $table) {
                $table->id();
                $table->timestamps();
                $table->softDeletes();
                $table->string('page_type')->default('CMS');
                $table->string('title');
                $table->string('url');
                $table->longText('content')->nullable();
                $table->string('component')->nullable();
                $table->integer('parent_id')->nullable();
                $table->string('entity_class')->nullable();
                $table->text('meta')->nullable();
                $table->integer('sort_index')->nullable();

                $table->string('uuid')->nullable();
                $table->timestamp('published_at')->nullable();
                $table->boolean('is_published')->default(false);
                $table->boolean('is_current')->default(false);
                $table->nullableMorphs('publisher');

                $table->index(['uuid', 'is_published', 'is_current']);
            });
        }

        if (!Schema::hasTable('menus')) {
            Schema::create('menus', function (Blueprint $table) {
                $table->id();
                $table->timestamps();
                $table->string('title');
                $table->string('slug');
                $table->text('items');
            });
        }

        if (!Schema::hasTable('seo')) {
            Schema::create('seo', function (Blueprint $table) {
                $table->id();
                $table->timestamps();
                $table->string('seoable_id');
                $table->string('seoable_type');
                $table->string('slug')->nullable();
                $table->string('meta_description')->nullable();
                $table->string('meta_title')->nullable();
                $table->string('robots')->nullable();
                $table->string('open_graph_image')->nullable();
            });
        } else {
            if (Schema::getColumnType('seo', 'seoable_id') === 'int') {
                Schema::table('seo', function (Blueprint $table) {
                    $table->string('seoable_id')->change();
                });
            }
        }
    }

    public function down(): void
    {
        Schema::dropIfExists('pages');
        Schema::dropIfExists('menus');
        Schema::dropIfExists('seo');
    }
};
