<?php

namespace Ercos\ErcosCms;

use Ercos\ErcosCms\Commands\CreateSectionCommand;
use Ercos\ErcosCms\Http\Controllers\MenuController;
use Ercos\ErcosCms\Http\Controllers\PageController;
use Filament\Support\Assets\Css;
use Filament\Support\Assets\Js;
use Filament\Support\Facades\FilamentAsset;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Spatie\LaravelPackageTools\Commands\InstallCommand;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class ErcosCmsServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('ercos-cms')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigrations([
                'create_ercos_cms_tables',
                'add_page_type_pdf_to_pages',
            ])
            ->hasCommands([
                CreateSectionCommand::class,
            ])
            ->hasInstallCommand(function (InstallCommand $command) {
                $command
                    ->publishConfigFile()
                    ->publishMigrations()
                    ->askToRunMigrations()
                    ->copyAndRegisterServiceProviderInApp();
            });
    }

    public function packageRegistered()
    {
        Route::prefix('api')->group(function () {
            Route::get('pages', [PageController::class, 'index']);
            Route::get('pages/preview', [PageController::class, 'preview']);
            Route::get('pages/published-urls', [PageController::class, 'listPublishedPageUrls']);
            Route::get('pages/{slug}', [PageController::class, 'show']);
            Route::get('sitemap', [PageController::class, 'sitemap']);
            Route::get('menus', [MenuController::class, 'index']);
        });
    }

    public function packageBooted()
    {
        if (config('ercos-cms.fontawesome')) {
            FilamentAsset::register([
                Css::make('fontawesome', config('ercos-cms.fontawesome')),
            ]);
        }

        if (config('ercos-cms.tiny_mce.script')) {
            FilamentAsset::register([
                Js::make('tiny-mce', config('ercos-cms.tiny_mce.script')),
            ]);
        }

        FilamentAsset::register([
            Js::make('fontawesome-plugin', __DIR__.'/../resources/js/tinymce-fontawesome.plugin.js'),
            Css::make('fontawesome-plugin', __DIR__.'/../resources/css/fontawesome-plugin.css')
        ], 'ercos-cms');

        /**
         * Drafts
         */
        if (method_exists($this->app['db']->connection()->getSchemaBuilder(), 'useNativeSchemaOperationsIfPossible')) {
            Schema::useNativeSchemaOperationsIfPossible();
        }

        Blueprint::macro('drafts', function (
            string $uuid = null,
            string $publishedAt = null,
            string $isPublished = null,
            string $isCurrent = null,
            string $publisherMorphName = null,
        ) {
            /** @var Blueprint $this */
            $uuid ??= config('ercos-cms.drafts.column_names.uuid', 'uuid');
            $publishedAt ??= config('ercos-cms.drafts.column_names.published_at', 'published_at');
            $isPublished ??= config('ercos-cms.drafts.column_names.is_published', 'is_published');
            $isCurrent ??= config('ercos-cms.drafts.column_names.is_current', 'is_current');
            $publisherMorphName ??= config('ercos-cms.drafts.column_names.publisher_morph_name',
                'publisher_morph_name');

            $this->uuid($uuid)->nullable();
            $this->timestamp($publishedAt)->nullable();
            $this->boolean($isPublished)->default(false);
            $this->boolean($isCurrent)->default(false);
            $this->nullableMorphs($publisherMorphName);

            $this->index([$uuid, $isPublished, $isCurrent]);
        });

        Blueprint::macro('dropDrafts', function (
            string $uuid = null,
            string $publishedAt = null,
            string $isPublished = null,
            string $isCurrent = null,
            string $publisherMorphName = null,
        ) {
            /** @var Blueprint $this */
            $uuid ??= config('ercos-cms.drafts.column_names.uuid', 'uuid');
            $publishedAt ??= config('ercos-cms.drafts.column_names.published_at', 'published_at');
            $isPublished ??= config('ercos-cms.drafts.column_names.is_published', 'is_published');
            $isCurrent ??= config('ercos-cms.drafts.column_names.is_current', 'is_current');
            $publisherMorphName ??= config('ercos-cms.drafts.column_names.publisher_morph_name',
                'publisher_morph_name');

            $this->dropIndex([$uuid, $isPublished, $isCurrent]);
            $this->dropMorphs($publisherMorphName);

            $this->dropColumn([
                $uuid,
                $publishedAt,
                $isPublished,
                $isCurrent,
            ]);
        });
    }
}
