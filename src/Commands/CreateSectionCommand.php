<?php

namespace Ercos\ErcosCms\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class CreateSectionCommand extends Command
{
    public $signature = 'ercos-cms:make:section {sectionName}';

    public $description = 'Creates a page section with the default template.';

    public function handle(): int
    {
        $sectionName = $this->argument('sectionName').'Section';

        $sectionClassName = Str::studly($sectionName);
        if (config('ercos-cms.section.pascal_case_selector')) {
            $sectionSelectorName = $sectionClassName;
        } else {
            $sectionSelectorName = Str::kebab($sectionName);
        }
        $suffix = config('ercos-cms.section.suffix');

        $sectionContent = <<<EOT
        <?php

        namespace App\Filament\Sections;

        class {$sectionClassName}
        {
            public static \$title = '{$sectionName}';
            public static \$selector = '{$sectionSelectorName}{$suffix}';

            public static function schema(): array
            {
                return [
                    // your form inputs
                ];
            }
        }
        EOT;

        $sectionFilePath = app_path("Filament/Sections/{$sectionClassName}.php");

        if (File::exists($sectionFilePath)) {
            $this->error('Section already exists.');
            return self::FAILURE;
        }

        File::put($sectionFilePath, $sectionContent);

        $this->info("Section '{$sectionName}' created successfully.");

        return self::SUCCESS;
    }
}
