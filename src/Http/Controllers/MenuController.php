<?php

namespace Ercos\ErcosCms\Http\Controllers;

use Ercos\ErcosCms\Facades\ErcosCms;

class MenuController
{
    public function index()
    {
        return response()->json([
            'data' => ErcosCms::getMenus()
        ]);
    }
}
