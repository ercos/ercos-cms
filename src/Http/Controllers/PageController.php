<?php

namespace Ercos\ErcosCms\Http\Controllers;

use Ercos\ErcosCms\Enums\PageType;
use Ercos\ErcosCms\Facades\ErcosCms;
use Ercos\ErcosCms\Filament\Plugin\CmsPlugin;
use Ercos\ErcosCms\Models\Page;
use Illuminate\Http\Request;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class PageController
{
    public function preview(Request $request)
    {
        $page = CmsPlugin::get()->getPageModel()::onlyDrafts()
            ->whereUrl($request->query('slug'))
            ->first();

        return response()->json([
            'data' => $page
        ]);
    }

    public function show($slug, Request $request)
    {
        $page = CmsPlugin::get()->getPageModel()::withoutDrafts(true)
            ->whereUrl('/'.$slug)
            ->with('seo')
            ->firstOrFail();

        return response()->json([
            'data' => $page
        ]);
    }

    public function index()
    {
        return response()->json([
            'data' => array_values(ErcosCms::getPages()->toArray())
        ]);
    }

    public function listPublishedPageUrls()
    {
        $pages = CmsPlugin::get()->getPageModel()::withoutDrafts(true)
            ->get()
            ->filter(fn(Page $page) => $page->page_type !== PageType::Collection->value)
            ->map(fn(Page $page) => $page->url);

        CmsPlugin::get()->getPageModel()::withoutDrafts(true)
            ->get()
            ->filter(fn(Page $page) => $page->page_type === PageType::Collection->value)
            ->each(function (Page $page) use ($pages) {
                $modelInstance = app($page->entity_class);
                foreach ($modelInstance->all() as $model) {
                    $replacement = '#:slug#';
                    if (config('ercos-cms.router.param_syntax') === 'braces') {
                        $replacement = '#{slug}#';
                    }
                    $pages[] = preg_replace($replacement, $model->slug, $page->url);
                }
            });

        return response()->json([
            'data' => array_values($pages->toArray())
        ]);
    }

    public function sitemap()
    {
        $sitemap = Sitemap::create();
        foreach (CmsPlugin::get()->getPageModel()::all() as $page) {
            if ($page->publishedPage) {
                $sitemap->add(
                    Url::create(getconfig('app.front_end_url').$page->url)
                        ->setLastModificationDate($page->updated_at)
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)
                        ->setPriority(0.8)
                );
            }
        }

        return $sitemap->render();
    }
}
