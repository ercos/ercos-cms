<?php

namespace Ercos\ErcosCms\Concerns;

use Illuminate\Database\Eloquent\Builder;

trait Draftable
{
    /**
     * Extends the eloquent query to include drafts.
     *
     * @return Builder
     */
    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->where('is_current', true)->withDrafts(true);
    }
}
