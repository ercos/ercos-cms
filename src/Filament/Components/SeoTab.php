<?php

namespace Ercos\ErcosCms\Filament\Components;

use Ercos\ErcosCms\Models\Page;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Tabs\Tab;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;

class SeoTab
{
    public static function schema($className): Tab
    {
        return Tab::make('SEO')
            ->schema([
                Grid::make('Seo')
                    ->relationship('seo')
                    ->schema([
                        TextInput::make('meta_title')
                            ->label('Titre')
                            ->helperText('Titre de la page. La longueur optimale est de 55 caractères.')
                            ->maxLength(80),
                        Textarea::make('meta_description')
                            ->label('Meta description')
                            ->helperText('Décrivez le contenu de la page. La longueur optimale est entre 155 et 300 caractères.')
                            ->maxLength(300),
                        Toggle::make('robots')
                            ->label('Indexation de la page')
                            ->formatStateUsing(function (Page $page) {
                                return !$page->robots;
                            })
                            ->dehydrateStateUsing(fn($state
                            ): ?string => $state ? null : 'noindex')
                            ->helperText('Si cette option est activée, la page sera ajoutée au sitemap et référencée sur les moteurs de recherche.'),
                        FileUpload::make('open_graph_image')
                            ->label('Image Open Graph')
                            ->helperText("Cette image s'affiche quand vous partagez du contenu sur Facebook, X (Twitter), LinkedIn et Pinterest.")
                            ->imageEditor()
                            ->imageCropAspectRatio('191:100')
                            ->acceptedFileTypes(['image/webp'])
                    ])
            ]);
    }
}
