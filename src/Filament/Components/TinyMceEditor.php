<?php

namespace Ercos\ErcosCms\Filament\Components;

use Filament\Forms\Components\Concerns;
use Filament\Forms\Components\Contracts;
use Filament\Forms\Components\Field;
use Illuminate\Support\Facades\Hash;

class TinyMceEditor extends Field implements Contracts\HasFileAttachments
{
    use Concerns\HasFileAttachments;

    protected string $view = 'ercos-cms::forms.components.tiny-mce-editor';

    public function getIdHash()
    {
        return Hash::make($this->getId());
    }

    public function getFileAttachmentsDirectory(): ?string
    {
        return filled($directory = $this->evaluate($this->fileAttachmentsDirectory))
            ? $directory
            : 'upload';
    }
}
