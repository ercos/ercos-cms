<?php

/**
 * Class SectionBase
 *
 * A base class for creating section components.
 * If applicable, the section can disable the customization of padding and the container.
 * For example, if the hero section is always "full width", container will be disabled.
 */

namespace Ercos\ErcosCms\Filament\Components;

use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;

class SectionBase
{
    public static function schema(
        string $forcePadding = '',
        string $forceContainer = '',
        bool $disableAnchor = false,
    ): Grid {
        $schema = [];

        $schema[] = Select::make('paddingY')
            ->label('Espacement vertical')
            ->required()
            ->options([
                'none' => 'Aucun',
                'small' => 'Condensé (small)',
                'medium' => 'Par défaut (medium)',
                'large' => 'Espacé (large)',
            ])
            ->default(strlen($forcePadding) > 0 ? $forcePadding : 'medium')
            // TODO add disabled avant déploiement en prod ->disabled($forcePadding)
            ->columnSpan(1);

        $schema[] = Select::make('container')
            ->label('Largeur du conteneur')
            ->required()
            ->default(strlen($forceContainer) > 0 ? $forceContainer : 'container')
            // TODO add disabled avant déploiement en prod ->disabled($forceContainer)
            ->options([
                'small' => 'Étroit',
                'container' => 'Par défaut',
                'fluid' => 'Fluide (95%)',
                'none' => 'Bord à bord (100%)',
            ])
            ->columnSpan(1);

        if (!$disableAnchor) {
            $schema[] = TextInput::make('anchor')
                ->label('ID de section')
                ->hint('Lien scrollable')
                ->columnSpan(1);
        }

        return Grid::make(3)
            ->schema($schema);
    }
}
