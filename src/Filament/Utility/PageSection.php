<?php

namespace Ercos\ErcosCms\Filament\Utility;

interface PageSection
{
    public static function schema(): array;
}
