<?php

namespace Ercos\ErcosCms\Filament\Utility;

use Filament\Forms\Components\Group;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use ReflectionClass;
use Symfony\Component\Finder\SplFileInfo;


/**
 * Class PageSectionLoader
 *
 * This class is responsible for loading available page sections into the page builder (PageResource) and providing related functionalities.
 */
class PageSectionLoader
{
    public static function getSections(): Collection
    {
        return static::getSectionClasses()->mapWithKeys(function ($class) {
            $reflectionClass = new ReflectionClass($class);

            return [
                $reflectionClass->getProperty('selector')->getValue() => $reflectionClass->getProperty('title')->getValue()
            ];
        });
    }

    public static function getSectionClasses(): Collection
    {
        $filesystem = app(Filesystem::class);

        return collect($filesystem->allFiles(app_path('Filament/Sections')))
            ->map(function (SplFileInfo $file): string {
                return (string) Str::of('App\\Filament\\Sections')
                    ->append('\\', $file->getRelativePathname())
                    ->replace(['/', '.php'], ['\\', '']);
            });
    }

    public static function getSectionsSchemas(): array
    {
        return static::getSectionClasses()
            ->map(fn($class) => Group::make($class::schema())
                ->columnSpanFull()
                ->afterStateHydrated(fn($component, $state) => $component->getChildComponentContainer()->fill($state))
                ->statePath(static::getSectionSelector($class))
                ->visible(function ($component, $get) use ($class) {
                    return $get('section') === static::getSectionSelector($class);
                })
            )
            ->toArray();
    }

    public static function getSectionSchema(string $sectionName): array
    {
        return static::getSectionClasses()
            ->first(function ($section) use ($sectionName) {
                return str_contains(static::getSectionSelector($section), $sectionName);
            })
            ::schema();
    }

    public static function getSectionSelector($class)
    {
        return (new ReflectionClass($class))->getProperty('selector')->getValue();
    }
}
