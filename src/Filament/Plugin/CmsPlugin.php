<?php

namespace Ercos\ErcosCms\Filament\Plugin;

use Closure;
use Ercos\ErcosCms\Filament\Resources\MenuResource;
use Ercos\ErcosCms\Filament\Resources\PageResource;
use Ercos\ErcosCms\Models\Menu;
use Ercos\ErcosCms\Models\Page;
use Filament\Contracts\Plugin;
use Filament\Panel;
use Filament\Support\Concerns\EvaluatesClosures;

class CmsPlugin implements Plugin
{
    use EvaluatesClosures;

    protected string|Closure|null $navigationGroup = null;
    protected string|null $menuModel = null;
    protected string|null $pageModel = null;
    protected bool|Closure $shouldRegisterNavigation = true;

    public static function make(): static
    {
        return app(static::class);
    }

    public function getId(): string
    {
        return 'ercos-cms';
    }

    public function register(Panel $panel): void
    {
        $panel
            ->resources([
                PageResource::class,
                MenuResource::class,
            ])
            ->pages([]);
    }

    public function boot(Panel $panel): void
    {

    }

    public function navigationGroup(string|Closure|null $group = null): static
    {
        $this->navigationGroup = $group;

        return $this;
    }

    public function menuModel(string $menuModel): static
    {
        $this->menuModel = $menuModel;

        return $this;
    }

    public function pageModel(string $pageModel): static
    {
        $this->pageModel = $pageModel;

        return $this;
    }

    public function registerNavigation(bool|Closure $show = true): static
    {
        $this->shouldRegisterNavigation = $show;

        return $this;
    }

    public static function get(): Plugin
    {
        return filament(app(static::class)->getId());
    }

    public function getNavigationGroup(): ?string
    {
        return $this->evaluate($this->navigationGroup) ?? config('ercos-cms.navigation_group');
    }

    public function getMenuModel(): Menu
    {
        return app($this->evaluate($this->menuModel) ?? config('ercos-cms.models.menu'));
    }

    public function getPageModel(): Page
    {
        return app($this->evaluate($this->pageModel) ?? config('ercos-cms.models.page'));
    }

    public function shouldRegisterNavigation(): bool
    {
        return $this->evaluate($this->shouldRegisterNavigation);
    }
}
