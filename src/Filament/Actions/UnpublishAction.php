<?php

namespace Ercos\ErcosCms\Filament\Actions;

use Filament\Actions\Action;

class UnpublishAction extends Action
{
    public static function getDefaultName(): ?string
    {
        return 'unpublish';
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->color('gray')
            ->visible(fn() => $this->getLivewire()->record->isPublished())
            ->requiresConfirmation()
            ->action($this->unpublish(...))
            ->label('Annuler la publication');
    }

    public function unpublish(): void
    {
        $record = $this->getLivewire()->record;
        $record::withoutTimestamps(fn() => $record->update(['is_published' => false]));
    }
}

