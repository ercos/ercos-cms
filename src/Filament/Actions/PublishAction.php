<?php

namespace Ercos\ErcosCms\Filament\Actions;

use Filament\Actions\Action;

class PublishAction extends Action
{
    public static function getDefaultName(): ?string
    {
        return 'publish';
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->color('success')
            ->action('publish')
            ->label('Publier');
    }
}
