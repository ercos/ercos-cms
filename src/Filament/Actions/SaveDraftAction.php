<?php

namespace Ercos\ErcosCms\Filament\Actions;

use Filament\Actions\Action;
use Filament\Resources\Pages\CreateRecord;
use Filament\Resources\Pages\EditRecord;
use Illuminate\Support\Str;

class SaveDraftAction extends Action
{
    public static function getDefaultName(): ?string
    {
        return 'draft';
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->action($this->draft(...))
            ->label('Sauvegarder le brouillon');
    }

    protected function draft()
    {
        $livewire = $this->getLivewire();
        $livewire->shouldSaveAsDraft = true;

        if ($livewire instanceof CreateRecord) {
            $livewire->create();
        }

        if ($livewire instanceof EditRecord) {
            $livewire->save();
            $livewire->redirect(
                route(
                    'filament.admin.resources.'.Str::slug($livewire->getRecord()->getTable()).'.edit',
                    ['record' => $livewire->getRecord()->revisions()->withDrafts(true)->current()->first()]
                )
            );
        }
    }

}
