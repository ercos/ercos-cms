<?php

namespace Ercos\ErcosCms\Filament\Resources;

use Ercos\ErcosCms\Concerns\Draftable;
use Ercos\ErcosCms\Enums\PageType;
use Ercos\ErcosCms\Enums\PublishingStatus;
use Ercos\ErcosCms\Filament\Components\SeoTab;
use Ercos\ErcosCms\Filament\Plugin\CmsPlugin;
use Ercos\ErcosCms\Filament\Resources\PageResource\Pages;
use Ercos\ErcosCms\Filament\Utility\PageSectionLoader;
use Ercos\ErcosCms\Models\Page;
use Filament\Forms;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Forms\Set;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;

class PageResource extends Resource
{
    use Draftable;

    protected static ?string $navigationIcon = 'heroicon-o-document-duplicate';

    protected static ?string $label = 'Pages';

    public static function getNavigationGroup(): ?string
    {
        return CmsPlugin::get()->getNavigationGroup();
    }

    public static function getModel(): string
    {
        return config('ercos-cms.models.page');
    }

    public static function form(Form $form): Form
    {
        $sections = PageSectionLoader::getSections();
        $schemas = PageSectionLoader::getSectionsSchemas();

        return $form
            ->schema([
                Forms\Components\Grid::make(12)
                    ->schema([
                        Tabs::make('Tabs')
                            ->tabs([
                                Tabs\Tab::make('Général')
                                    ->schema([
                                        TextInput::make('title')
                                            ->label('Titre')
                                            ->helperText('Titre dans la navigation ou le pied de page.')
                                            ->live(true)
                                            ->afterStateUpdated(function (
                                                Get $get,
                                                Set $set,
                                                ?string $old,
                                                ?string $state
                                            ) {
                                                if (($get('url') ?? '') !== Str::slug($old)) {
                                                    return;
                                                }

                                                $set('url', '/'.Str::slug($state));
                                            })
                                            ->required()
                                            ->columnSpanFull(),
                                        TextInput::make('url')
                                            ->label('URL')
                                            ->helperText('Doit commencer par /, ex : /ma-page')
                                            ->required()
                                            ->startsWith('/')
                                            ->unique(
                                                ignoreRecord: true,
                                                modifyRuleUsing: fn($rule) => $rule
                                                    ->whereNull(
                                                        method_exists(CmsPlugin::get()->getPageModel(),
                                                            'getDeletedAtColumn')
                                                            ? CmsPlugin::get()->getPageModel()->getDeletedAtColumn()
                                                            : 'deleted_at'
                                                    )
                                                    ->where('is_current', true)
                                            )
                                            ->disabled(fn(Get $get) => strlen($get('entity_class')) > 0)
                                            ->dehydrated()
                                            ->columnSpanFull(),
                                    ])
                                    ->columnSpan(2),
                                SeoTab::schema(CmsPlugin::get()->getPageModel()::class),
                                Tabs\Tab::make('Avancé')
                                    ->visible(fn() => Auth::user()->is_super_admin)
                                    ->schema([
                                        Forms\Components\KeyValue::make('meta')
                                            ->keyPlaceholder('internal_id')
                                    ])
                            ])
                            ->columnSpan(9),
                        Forms\Components\Section::make('Type de page')
                            ->schema([
                                Select::make('page_type')
                                    ->label('Type')
                                    ->required()
                                    ->reactive()
                                    ->options(Auth::user()->is_super_admin
                                        ? [
                                            PageType::CMS->value => 'Page CMS',
                                            PageType::PDF->value => 'Document PDF',
                                            PageType::Custom->value => 'Page personnalisée',
                                            PageType::Collection->value => 'Page de collection'
                                        ] : [
                                            PageType::CMS->value => 'Page CMS',
                                            PageType::PDF->value => 'Document PDF',
                                        ]
                                    )
                                    ->default(PageType::CMS->value)
                                    ->selectablePlaceholder(false),
                                Forms\Components\FileUpload::make('file')
                                    ->label('Document PDF')
                                    ->acceptedFileTypes(['application/pdf'])
                                    ->preserveFilenames()
                                    ->visible(fn(Get $get) => $get('page_type') === PageType::PDF->value)
                                    ->columnSpanFull(),
                                TextInput::make('component')
                                    ->label('Composant / Vue')
                                    ->required()
                                    ->visible(fn(Get $get
                                    ) => $get('page_type') === PageType::Custom->value || $get('page_type') === PageType::Collection->value)
                                    ->disabled(fn() => !Auth::user()->is_super_admin)
                                    ->columnSpanFull(),
                                Select::make('entity_class')
                                    ->label('Entité')
                                    ->required()
                                    ->options(config('ercos-cms.collection_page_model_options'))
                                    ->visible(fn(Get $get) => $get('page_type') === PageType::Collection->value)
                                    ->disabled(fn() => !Auth::user()->is_super_admin),
                                Select::make('parent_id')
                                    ->label('Page parente de la collection')
                                    ->reactive()
                                    ->afterStateUpdated(function (
                                        Set $set,
                                        ?string $state
                                    ) {
                                        $page = CmsPlugin::get()->getPageModel()::findOrFail($state);
                                        if (config('ercos-cms.router.param_syntax') === 'braces') {
                                            $set('url', "$page->url/{slug}");
                                        } else {
                                            $set('url', "$page->url/:slug");
                                        }
                                    })
                                    ->required()
                                    ->options(CmsPlugin::get()->getPageModel()::all()->pluck('title', 'id'))
                                    ->visible(fn(Get $get) => $get('page_type') === PageType::Collection->value)
                                    ->disabled(fn() => !Auth::user()->is_super_admin)
                                    ->searchable(),
                            ])
                            ->columnSpan(3),
                        Forms\Components\Grid::make(1)
                            ->schema([
                                Repeater::make('content')
                                    ->label('Contenu')
                                    ->addActionLabel('Ajouter une section')
                                    ->collapsible()
                                    ->collapsed(true)
                                    ->schema([
                                        Select::make('section')
                                            ->label('Section')
                                            ->placeholder('Sélectionnez une section')
                                            ->required()
                                            ->live()
                                            ->searchable()
                                            ->columnSpanFull()
                                            ->options($sections),
                                        ...$schemas
                                    ])
                                    ->itemLabel(fn(array $state): ?string => $state['section']
                                        ? $sections->get($state['section'])
                                        : 'Nouvelle section'
                                    )
                            ])
                            ->visible(fn(Get $get) => $get('page_type') === PageType::CMS->value)
                    ])
                    ->columnSpanFull()
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->label('Titre')
                    ->sortable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('url')
                    ->label('URL')
                    ->sortable()
                    ->searchable(),
                TextColumn::make('status')
                    ->label('Statut')
                    ->badge()
                    ->formatStateUsing(fn(PublishingStatus $state): string => match ($state) {
                        PublishingStatus::Draft => 'Brouillon',
                        PublishingStatus::Review => 'À publier',
                        PublishingStatus::Published => 'Publié',
                    })
                    ->color(fn(PublishingStatus $state): string => match ($state) {
                        PublishingStatus::Draft => 'gray',
                        PublishingStatus::Review => 'info',
                        PublishingStatus::Published => 'success',
                    }),
                Tables\Columns\TextColumn::make('page_type')
                    ->label('Type')
                    ->formatStateUsing(fn(string $state): string => match ($state) {
                        PageType::CMS->value => 'CMS',
                        PageType::Custom->value => 'Personnalisée',
                        PageType::Collection->value => 'Collection',
                        PageType::PDF->value => 'PDF',
                    }),
            ])
            ->reorderable('sort_index')
            ->defaultSort('sort_index')
            ->defaultPaginationPageOption(25)
            ->actions([
                Tables\Actions\Action::make('duplicate')
                    ->label('Dupliquer')
                    ->icon('heroicon-o-document-duplicate')
                    ->requiresConfirmation()
                    ->modalHeading('Dupliquer la page')
                    ->modalDescription(fn(Page $record
                    ) => new HtmlString("Êtes-vous sûr·e de vouloir dupliquer la page <b>$record->title</b> ?"))
                    ->modalSubmitActionLabel('Dupliquer')
                    ->action(function (Page $record): void {
                        $duplicatedPage = $record->replicate();
                        $duplicatedPage->uuid = Str::uuid()->toString();
                        $duplicatedPage->title = $record->title.' - Copie';
                        $duplicatedPage->url = $record->url.'-copie';
                        $duplicatedPage->saveAsDraft();
                    }),
                Tables\Actions\DeleteAction::make()
                    ->modalHeading('Supprimer la page')
                    ->modalDescription(fn($record
                    ) => new HtmlString("Êtes-vous sûr·e de vouloir supprimer la page <b>$record->title</b> ?"))
                    ->modalSubmitActionLabel('Oui, supprimer')
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPages::route('/'),
            'create' => Pages\CreatePage::route('/create'),
            'edit' => Pages\EditPage::route('/{record}/edit'),
        ];
    }

    public static function shouldRegisterNavigation(): bool
    {
        return CmsPlugin::get()->shouldRegisterNavigation();
    }
}



