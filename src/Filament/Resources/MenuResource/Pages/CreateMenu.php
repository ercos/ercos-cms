<?php

namespace Ercos\ErcosCms\Filament\Resources\MenuResource\Pages;

use Ercos\ErcosCms\Filament\Resources\MenuResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateMenu extends CreateRecord
{
    protected static string $resource = MenuResource::class;
}
