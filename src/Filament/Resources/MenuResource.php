<?php

namespace Ercos\ErcosCms\Filament\Resources;

use Ercos\ErcosCms\Filament\Plugin\CmsPlugin;
use Ercos\ErcosCms\Filament\Resources\MenuResource\Pages;
use Filament\Forms\Components\Builder;
use Filament\Forms\Components\Checkbox;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class MenuResource extends Resource
{
    protected static ?string $navigationIcon = 'heroicon-m-bars-4';
    protected static ?string $label = 'Navigation';
    protected static ?string $pluralLabel = 'Navigation';

    public static function getNavigationGroup(): ?string
    {
        return CmsPlugin::get()->getNavigationGroup();
    }

    public static function getModel(): string
    {
        return config('ercos-cms.models.menu');
    }

    public static function form(Form $form): Form
    {
        $pageSchema = [
            Select::make('page_uuid')
                ->label('Page')
                ->required()
                ->options(CmsPlugin::get()->getPageModel()::withoutDrafts()->pluck('title', 'uuid'))
                ->searchable(),
            Checkbox::make('enable_anchor')
                ->live()
                ->default(false)
                ->label("Le lien doit pointer vers une section de la page"),
            TextInput::make('anchor')
                ->label('ID de section')
                ->visible(fn(Get $get) => $get('enable_anchor'))
                ->required()
        ];

        return $form
            ->schema([
                TextInput::make('title')
                    ->required(),
                TextInput::make('slug')
                    ->required(),
                Builder::make('items')
                    ->label('Éléments du menu')
                    ->addActionLabel('Ajouter un élément de menu')
                    ->blocks([
                        Builder\Block::make('page')
                            ->label('Page')
                            ->schema($pageSchema),
                        Builder\Block::make('menu')
                            ->label('Menu avec sous-menus')
                            ->schema([
                                TextInput::make('title')
                                    ->label('Titre du menu')
                                    ->maxLength(30)
                                    ->required(),
                                Repeater::make('items')
                                    ->schema($pageSchema)
                                    ->maxItems(15)
                                    ->reorderable()
                                    ->label('Éléments du sous-menu')
                                    ->addActionLabel('Ajouter un élément de sous-menu')
                            ])

                    ])
                    ->columnSpanFull(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title'),
                Tables\Columns\TextColumn::make('slug'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMenus::route('/'),
            'create' => Pages\CreateMenu::route('/create'),
            'edit' => Pages\EditMenu::route('/{record}/edit'),
        ];
    }

    public static function shouldRegisterNavigation(): bool
    {
        return CmsPlugin::get()->shouldRegisterNavigation();
    }
}
