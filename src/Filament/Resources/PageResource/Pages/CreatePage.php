<?php

namespace Ercos\ErcosCms\Filament\Resources\PageResource\Pages;

use Ercos\ErcosCms\Filament\Pages\Create\DraftableCreate;
use Ercos\ErcosCms\Filament\Resources\PageResource;
use Filament\Resources\Pages\CreateRecord;

class CreatePage extends CreateRecord
{
    use DraftableCreate;

    protected static string $resource = PageResource::class;

    public static ?string $title = 'Créer une page';
}
