<?php

namespace Ercos\ErcosCms\Filament\Resources\PageResource\Pages;

use Ercos\ErcosCms\Enums\PageType;
use Ercos\ErcosCms\Enums\PublishingStatus;
use Ercos\ErcosCms\Filament\Pages\Edit\DraftableEdit;
use Ercos\ErcosCms\Filament\Resources\PageResource;
use Ercos\ErcosCms\Models\Page;
use Filament\Actions\Action;
use Filament\Resources\Pages\EditRecord;

class EditPage extends EditRecord
{
    use DraftableEdit;

    protected static string $resource = PageResource::class;

    public static ?string $title = 'Modifier page';

    protected function getHeaderActions(): array
    {
        return [
            Action::make('preview')
                ->label('Prévisualiser')
                ->color('gray')
                ->visible(fn() => $this->getRecord()->page_type === PageType::CMS->value)
                ->url(function () {
                    /** @var Page $page */
                    $page = $this->getRecord();
                    if ($page->status === PublishingStatus::Published) {
                        return $page->live_url;
                    } else {
                        return $page->preview_url;
                    }
                })
                ->openUrlInNewTab(),
        ];
    }
}
