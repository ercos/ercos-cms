<?php

namespace Ercos\ErcosCms\Filament\Pages\Create;

use Ercos\ErcosCms\Filament\Actions\SaveDraftAction;
use Filament\Actions\Action;
use Illuminate\Database\Eloquent\Model;

trait DraftableCreate
{
    public bool $shouldSaveAsDraft = false;

    protected function handleRecordCreation(array $data): Model
    {
        $model = $this->getModel()::make([
            ...$data,
            'is_published' => !$this->shouldSaveAsDraft,
        ]);

        $model->withoutRevision()->save();

        return $model;
    }

    protected function getCreateFormAction(): Action
    {
        return parent::getCreateFormAction()
            ->color('success')
            ->label('Publier');
    }

    protected function getFormActions(): array
    {
        return [
            ...array_slice(parent::getFormActions(), 0, 1),
            SaveDraftAction::make(),
            ...array_slice(parent::getFormActions(), 1),
        ];
    }
}
