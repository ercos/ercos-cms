<?php

namespace Ercos\ErcosCms\Filament\Pages\Edit;

use Ercos\ErcosCms\Filament\Actions\SaveDraftAction;
use Ercos\ErcosCms\Filament\Actions\UnpublishAction;
use Filament\Actions\Action;
use Filament\Notifications\Notification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 *
 * @method Model getRecord()
 * @method string getResource()
 */
trait DraftableEdit
{
    public bool $shouldSaveAsDraft = false;

    protected function handleRecordUpdate(Model $record, array $data): Model
    {
        if ($record->isPublished() && $this->shouldSaveAsDraft) {
            $record->updateAsDraft($data);
        } elseif ($record->isPublished() && !$this->shouldSaveAsDraft) {
            $record->update($data);
        } elseif (!$record->is_current && $this->shouldSaveAsDraft) {
            $record->updateAsDraft($data);
        } else {
            // Unpublish all other revisions
            if (!$this->shouldSaveAsDraft) {
                /** @var HasMany $revisions */
                $record::withoutTimestamps(fn() => $record->revisions()
                    ->where('is_published', true)
                    ->update(['is_published' => false]));
            }

            $record->update([
                ...$data,
                'is_published' => !$this->shouldSaveAsDraft,
            ]);
        }

        if ($record->isPublished()) {
            $record->pruneRevisions();
        }

        return $record;
    }

    protected function getSaveFormAction(): Action
    {
        return parent::getSaveFormAction()
            ->color('success')
            ->label('Publier');
    }

    protected function getFormActions(): array
    {
        return [
            ...array_slice(parent::getFormActions(), 0, 1),
            SaveDraftAction::make(),
            UnpublishAction::make(),
            ...array_slice(parent::getFormActions(), 1),
        ];
    }

    protected function getSavedNotificationTitle(): ?string
    {
        return $this->shouldSaveAsDraft
            ? 'Brouillon sauvegardé'
            : 'Publié';
    }

    protected function getSavedNotification(): ?Notification
    {
        $notification = parent::getSavedNotification();

        $this->shouldSaveAsDraft = false;

        return $notification;
    }
}
