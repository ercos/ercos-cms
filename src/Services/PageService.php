<?php

namespace Ercos\ErcosCms\Services;

class PageService
{
    public function cleanContent(array $pageContent)
    {
        return array_map(function ($block) {
            return [
                'section' => $block['section'],
                $block['section'] => $block[$block['section']]
            ];
        }, $pageContent);
    }
}
