<?php

namespace Ercos\ErcosCms\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class FontawesomeService
{
    public function getHtmlElement(string $icon)
    {
        return "<span style='display: inline-block; width: 20px'><i class='far fa-".$icon."'></i></span><span> ".$icon."</span>";
    }

    /**
     * @param  string  $search
     * @param  int  $take
     * @return array|Collection
     */
    public function searchFontawesomeIconNamesOptions(string $search, int $take = 10)
    {
        if (strlen($search) < 2) {
            return [];
        }

        $query = <<<GQL
            query {
                search(version: "6.0.0", query: "$search", first: $take) { id }
            }
            GQL;

        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
        ])->post('https://api.fontawesome.com', [
            'query' => $query
        ]);

        $result = (new Collection($response->json()['data']['search']))->map(fn($icon) => [
            'label' => $this->getHtmlElement($icon['id']),
            'value' => $icon['id']
        ]);

        return $result;
    }
}