<?php

namespace Ercos\ErcosCms\Enums;

enum PublishingStatus: string
{
    case Draft = 'DRAFT';
    case Review = 'REVIEW';
    case Published = 'PUBLISHED';
}
