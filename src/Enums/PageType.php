<?php

namespace Ercos\ErcosCms\Enums;

enum PageType: string
{
    case CMS = 'CMS';
    case Custom = 'CUSTOM';
    case Collection = 'COLLECTION';
    case PDF = 'PDF';
}
