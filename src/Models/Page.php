<?php

namespace Ercos\ErcosCms\Models;

use Ercos\ErcosCms\Concerns\HasDrafts;
use Ercos\ErcosCms\Enums\PageType;
use Ercos\ErcosCms\Filament\Plugin\CmsPlugin;
use Ercos\ErcosCms\Services\PageService;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class Page extends Model
{
    use HasDrafts;
    use SoftDeletes;

    protected $fillable = [
        'title',
        'url',
        'content',
        'page_type',
        'entity_class',
        'parent_id',
        'component',
        'file',
        'meta',
        'is_published',
        'published_at',
    ];

    protected $casts = [
        'content' => 'json',
        'meta' => 'array',
    ];

    protected $appends = [
        'preview_url',
        'live_url',
    ];

    protected static function booted(): void
    {
        static::saving(function (Page $page) {
            // Le contenu est "mis au propre" en base, car sinon sauvegarde toutes les sections (mêmes cachées) => TODO en mettant disabled sur les champs ça devrait régler le problème ?
            $pageService = App::make(PageService::class);
            if ($page->page_type === PageType::CMS->value) {
                $page->content = $pageService->cleanContent($page->content);
            }

            // S'il y a une page enfant (pour le moment : collection uniquement), on met à jour l'URL de l'enfant
            if ($page->children()->count() > 0 && $page->isDirty('url')) {
                $page->children()->each(function (Page $childPage) use ($page) {
                    $oldUrl = $page->getOriginal('url');
                    $newUrl = $page->url;
                    $childPage->url = preg_replace("#$oldUrl#", $newUrl, $childPage->url);
                    $childPage->saveOrFail();
                });
            }
        });

        static::deleting(function (Page $page) {
            // Suppression dans les liens du menu
            $menus = CmsPlugin::get()->getMenuModel()::all();
            foreach ($menus as $menu) {
                $menu->items = collect($menu->items)
                    ->filter(function ($menuItem) use ($page) {
                        if ($menuItem['type'] === 'page') {
                            return $menuItem['data']['page_uuid'] != $page->uuid;
                        }

                        return true;
                    })
                    ->map(function ($menuItem) use ($page) {
                        if ($menuItem['type'] === 'page') {
                            return $menuItem;
                        }

                        return [
                            ...$menuItem,
                            'data' => [
                                ...$menuItem['data'],
                                'items' => collect($menuItem['data']['items'])
                                    ->filter(function ($id) use ($page) {
                                        return $id != $page->uuid;
                                    })
                                    ->toArray()
                            ]
                        ];
                    })
                    ->toArray();
                $menu->saveOrFail();
            }
        });
    }

    protected function previewUrl(): Attribute
    {
        return Attribute::get(fn() => config('ercos-cms.front_end_url').'/preview?slug='.$this->url);
    }

    protected function liveUrl(): Attribute
    {
        return Attribute::get(fn() => $this->is_published ? config('ercos-cms.front_end_url').$this->url : null);
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(CmsPlugin::get()->getPageModel()::class, 'parent_id', 'id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(CmsPlugin::get()->getPageModel()::class, 'parent_id', 'id');
    }

    public function seo(): MorphOne
    {
        return $this->morphOne(Seo::class, 'seoable', null, null, 'uuid');
    }
}
