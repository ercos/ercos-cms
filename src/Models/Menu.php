<?php

namespace Ercos\ErcosCms\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'items'
    ];

    protected $casts = [
        'items' => 'json'
    ];

}
