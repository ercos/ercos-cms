<?php

namespace Ercos\ErcosCms\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $table = 'seo';

    protected $fillable = [
        'slug',
        'meta_title',
        'meta_description',
        'robots',
        'open_graph_image'
    ];

    protected $appends = [
        'open_graph_image_url'
    ];

    protected function openGraphImageUrl(): Attribute
    {
        return Attribute::get(fn(
        ) => $this->open_graph_image ? config('app.url').'/storage/'.$this->open_graph_image : null);
    }
}
