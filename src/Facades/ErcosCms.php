<?php

namespace Ercos\ErcosCms\Facades;

use Ercos\ErcosCms\Filament\Plugin\CmsPlugin;
use Ercos\ErcosCms\Models\Menu;
use Ercos\ErcosCms\Models\Page;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Ercos\ErcosCms\ErcosCms
 */
class ErcosCms extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Ercos\ErcosCms\ErcosCms::class;
    }

    public static function getMenuItemsBySlug($slug): array
    {
        return self::getMenus()->first(fn($menu) => $menu['slug'] === $slug)['items'];
    }

    /**
     * @return Collection<Menu>
     */
    public static function getMenus(): Collection
    {
        return CmsPlugin::get()->getMenuModel()::get()
            ->map(function (Menu $menu) {
                return [
                    ...$menu->toArray(),
                    'items' => collect($menu->items)
                        ->map(function ($menuItem) {
                            if ($menuItem['type'] === 'page') {
                                $page = self::getPageByUuid($menuItem['data']['page_uuid']);
                            }

                            $data = [
                                'type' => isset($menuItem['data']['anchor']) ? 'anchor' : $menuItem['type'],
                                'title' => $page?->title ?? $menuItem['data']['title'] ?? null,
                                'page_type' => $page?->page_type ?? null,
                                'anchor' => $menuItem['data']['anchor'] ?? null,
                                'meta' => $menuItem['data']['meta'] ?? null
                            ];

                            if ($menuItem['type'] === 'page') {
                                return [
                                    ...$data,
                                    'url' => $page->url,
                                ];
                            } else {
                                if ($menuItem['type'] === 'menu') {
                                    return [
                                        ...$data,
                                        'items' => collect($menuItem['data']['items'])
                                            ->map(function ($pageItem) {
                                                if (is_string($pageItem)) {
                                                    $pageUuid = $pageItem;
                                                } else {
                                                    $pageUuid = $pageItem['page_uuid'];
                                                }
                                                $page = self::getPageByUuid($pageUuid);

                                                return [
                                                    'type' => 'page',
                                                    'title' => $page->title,
                                                    'page_type' => $page->page_type,
                                                    'anchor' => $pageItem['anchor'] ?? null,
                                                    'url' => $page->url,
                                                    'meta' => $page->meta,
                                                ];
                                            })
                                    ];
                                } else {
                                    return $data;
                                }
                            }
                        })
                        ->toArray()
                ];
            });
    }

    /**
     * @param $metaKey
     * @param $metaValue
     * @return Page
     */
    public static function getPageByMetaKey($metaKey, $metaValue): Page
    {
        return self::getPages()->first(fn(Page $page) => $page->meta && $page->meta[$metaKey] === $metaValue);
    }

    /**
     * @param $uuid
     * @return Page|null
     */
    public static function getPageByUuid($uuid): ?Page
    {
        return self::getPages()->first(fn(Page $page) => $page->uuid === $uuid);
    }

    /**
     * @return Collection<Page>
     */
    public static function getPages(): Collection
    {
        return CmsPlugin::get()->getPageModel()::withoutDrafts(true)
            ->with('seo')
            ->get()
            ->sort(fn($page) => str_contains($page->url, ':slug') ? 1 : -1);
    }
}
