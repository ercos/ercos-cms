<?php

namespace Ercos\ErcosCms\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \Illuminate\Contracts\Auth\Authenticatable getCurrentUser()
 * @method static void previewMode(bool $previewMode = true)
 * @method static void disablePreviewMode()
 * @method static bool isPreviewModeEnabled()
 * @method static void withDrafts(bool $withDrafts = true)
 * @method static bool isWithDraftsEnabled()
 *
 * @see \Ercos\ErcosCms\LaravelDrafts
 */
class LaravelDrafts extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Ercos\ErcosCms\LaravelDrafts::class;
    }
}