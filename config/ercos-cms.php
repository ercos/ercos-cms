<?php

return [
    'front_end_url' => env('FRONT_END_URL', 'http://localhost:4200'),

    'section' => [
        'pascal_case_selector' => true,
        'suffix' => 'Component'
    ],

    'router' => [
        // either colon (will generate /pages/:slug) or braces (will generate /pages/{slug})
        'param_syntax' => 'colon',
    ],

    // Dashboard menu group to add the buttons
    'navigation_group' => null,

    // In case you need to extend the base models
    'models' => [
        'page' => \Ercos\ErcosCms\Models\Page::class,
        'menu' => \Ercos\ErcosCms\Models\Menu::class
    ],

    // Models that may be used as collection page model
    'collection_page_model_options' => [
        // Article::class => 'Article',
        // Faq::class => 'FAQ',
    ],

    // Fontawesome kit (css). 'https://kit.fontawesome.com/********.css'
    'fontawesome' => null,

    // Configuration pour utiliser le TinyMceEditor
    'tiny_mce' => [
        // TinyMCE kit. 'https://cdn.tiny.cloud/1/********/tinymce/7/tinymce.min.js'
        'script' => null,

        // Available text formats
        'block_formats' => 'Paragraph=p;Header 2=h2;Header 3=h3',

        // Toolbars
        // In case fontawesome configuration is defined, the `fontawesome` plugin can be added
        'toolbar' => 'undo redo removeformat | blocks | bold italic | alignjustify alignright aligncenter alignleft | numlist bullist | forecolor backcolor | blockquote table toc hr | image link codesample emoticons | wordcount fullscreen'
    ],

    'drafts' => [
        'revisions' => [
            'keep' => 10,
        ],
        // Configuration for the draftable trait
        'column_names' => [
            /*
             * Boolean column that marks a row as the current version of the data for editing.
             */
            'is_current' => 'is_current',

            /*
             * Boolean column that marks a row as live and displayable to the public.
             */
            'is_published' => 'is_published',

            /*
             * Timestamp column that stores the date and time when the row was published.
             */
            'published_at' => 'published_at',

            /*
             * UUID column that stores the unique identifier of the model drafts.
             */
            'uuid' => 'uuid',

            /*
             * Name of the morph relationship to the publishing user.
             */
            'publisher_morph_name' => 'publisher',
        ],
    ]
];
